import Card from 'react-bootstrap/Card';
import { Link } from 'react-router-dom';
// import { Row, Col, Container } from 'react-bootstrap';

export default function ProductCard({productInfo}) {
	
const { _id, name, description, price } = productInfo;

	return(
		<Card className="mb-5 text-center" border="primary">
			<Card.Body className="mt-2 mb-2">
				<Card.Title> {name} </Card.Title>
				<Card.Subtitle> Product Description: </Card.Subtitle>
				<Card.Text>
						{description}
				</Card.Text>
				<Card.Subtitle> Product Price: </Card.Subtitle>
				<Card.Text> PHP {price} </Card.Text>
				<Link className="btn btn-primary" to={`/products/${_id}`}> See Details </Link>
			</Card.Body>
		</Card>
	);
}

