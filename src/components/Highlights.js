import { Row, Col, Card as Baraha } from 'react-bootstrap';

export default function Highlights() {
	return(
		<Row className="mt-3 mb-3">
			{/*1st Column*/}
			<Col xs={12} md={4}>
				<Baraha className="Highlights p-3">
					<Baraha.Body>
						<Baraha.Title>
							ONE YEAR WARRANTY
						</Baraha.Title>
						<Baraha.Text>
							If it breaks and you bought it from us we will repair, replace or refund the item. We will not make excuses. We will not make you present the original packaging, jump through hoops, slice off your index finger or any other bullsh*t practice that makes it difficult to make a warranty claim.
						</Baraha.Text>
					</Baraha.Body>
				</Baraha>
			</Col>
			{/*2nd Column*/}
			<Col xs={12} md={4}>
				<Baraha className="Highlights p-3">
					<Baraha.Body>
						<Baraha.Title>
							WHAT YOU SEE IS WHAT YOU GET
						</Baraha.Title>
						<Baraha.Text>
							Everything we display is in stock. No more waiting and downpaying and following up when the stock will arrive. You want something? Buy it and enjoy it. We do not believe in making the customer wait.
						</Baraha.Text>
					</Baraha.Body>
				</Baraha>
			</Col>
			{/*3rd Column*/}
			<Col xs={12} md={4}>
				<Baraha className="Highlights p-3">
					<Baraha.Body>
						<Baraha.Title>
							GET MORE THAN WHAT YOU BARGAINED FOR
						</Baraha.Title>
						<Baraha.Text>
							When you shop here in Hardware Sugar, you get WAY more than your money's worth. Earn discount points with every purchase!
						</Baraha.Text>
					</Baraha.Body>
				</Baraha>
			</Col>
		</Row>
	);
}