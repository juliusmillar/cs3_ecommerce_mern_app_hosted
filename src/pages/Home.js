import Hero from '../components/Banner';
import Showcase from '../components/Highlights';
import { Carousel } from 'react-bootstrap';

let details = {
	title: "Welcome to 3 Cards Computer Hub",
	tagline: "A MODERN STORE IN A MODERN WORLD. You demand the latest and cutting-edge tech? We welcome every PC tinkerer out there; if it's high-end or high-performance parts you're looking for, 3 Cards Computer Hub has it."

}

export default function Home () {
	return(
		<div className="container-home">
			<Hero info={details} />
			<Showcase />

			<Carousel className="mt-5 mb-5">
			  <Carousel.Item>
			    <img
			      className="d-block w-100"
			      src="https://images.unsplash.com/photo-1591799264318-7e6ef8ddb7ea?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1674&q=80"
			      alt="First slide"
			    />
			    <Carousel.Caption>
			      <h3>Central Processing Units</h3>
			      <p>Partner your CPU with compatible Motherboard with excellent upgrade path.</p>
			    </Carousel.Caption>
			  </Carousel.Item>
			  
			  <Carousel.Item>
			    <img
			      className="d-block w-100"
			      src="https://images.unsplash.com/photo-1591488320449-011701bb6704?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1170&q=80"
			      alt="Second slide"
			    />
			    <Carousel.Caption>
			      <h3>Graphics Processing Units</h3>
			      <p>Get good deals and discount when purchasing video cards without compromising the capacity.</p>
			    </Carousel.Caption>
			  </Carousel.Item>
			  
			  <Carousel.Item>
			    <img
			      className="d-block w-100"
			      src="https://images.unsplash.com/photo-1578091879915-33ee869e2cd7?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1302&q=80"
			      alt="Third slide"
			    />
			    <Carousel.Caption>
			      <h3>Desktop and Laptop Packages</h3>
			      <p>Acquire your dream computer by choosing from our pre-built and ready-made Computer Packages.</p>
			    </Carousel.Caption>
			  </Carousel.Item>
			</Carousel>

		</div>
	);
}

