import { useState } from 'react';
import ProductCard from '../components/ProductCard';
import { Row, Col, Container, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function DashBoard() {

	const [ products, setProducts ] = useState([]); 

	fetch('https://calm-sierra-46560.herokuapp.com/products/active').then(response => response.json()).then(convertedData => {
		console.log(convertedData);
		setProducts(convertedData.map(item => {
			return(
				<ProductCard key={item._id} productInfo={item}/>
			)
		}))
	})

	return(
		<Container>
			<Card className= " p-5 mb-5 mt-5 text-center font-weight-bold ">
				<h1>Admin Dashboard</h1>
				<Row className= " mt-5 font-weight-bold" >
					<Col>
						{/*<Button type="submit" id="submitBtn" variant="success" className="btn btn-block">Create New Product</Button>*/}
						<Link className="btn btn-success btn-block" to="/create">Create New Product</Link>
					</Col>
					<Col>
						<Button type="submit" id="submitBtn" variant="warning" className="btn btn-block">Update Existing Product</Button>
					</Col>
				</Row>
			</Card>
			{products}
		</Container>
	);
}