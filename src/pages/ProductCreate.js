import { useState, useEffect } from 'react';
import {Form, Button, Container } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useHistory } from 'react-router-dom';

export default function CreateProduct() {
	
		const history = useHistory();
		const [ name, setName ] = useState('');
		const [ description, setDescription ] = useState('');
		const [ price, setPrice ] = useState('');
		const [ isComplete, setComplete ] = useState(false);

		function productCreate(event) {
			event.preventDefault();

			console.log(name);
			console.log(description);
			console.log(price);

			fetch('https://calm-sierra-46560.herokuapp.com/products/create', {
				method: "POST",
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					name: name,
					description: description,
					price: price
				})
			})
			.then(res => res.json())
			.then(data => {
				if (data) {
					Swal.fire({
						title: `${name} is created successfully`,
						icon: 'success',
						text: 'Check Admin Dashboard!'
					})
					history.push('/dashboard');
				} else {
					Swal.fire({
						title: `Product Creation Failed`,
						icon: 'error',
						text: 'Please try to create product again!'
					})
				}
			})
		} 

		useEffect(() => {

			// creating a control strucutre that will validate the values of the input fields.
			if (name !== '' && description !== '' && price !== '') {
				setComplete(true);
			} else {
				setComplete(false);
			}
		}, [name, description, price]);

	return(
		<Container>

			{/*Register Fom*/}
			<Form onSubmit={(event) => productCreate(event) } className="mb-5">

				{/*Product Name*/}
				<Form.Group controlId="name">
					<Form.Label>
						Product Name:
					</Form.Label>
					<Form.Control
						type="text"
						placeholder="Insert Product Name Here" value={name}
						onChange={event => setName(event.target.value)}
						required
					/>
				</Form.Group>

				{/*Product Description*/}
				<Form.Group controlId="description">
					<Form.Label>
						Description:
					</Form.Label>
					<Form.Control
						type="text"
						placeholder="Insert Description Here" value={description}
						onChange={pangyayari => setDescription(pangyayari.target.value)}
						required
					/>
				</Form.Group>

				{/*Price*/}
				<Form.Group controlId="price">
					<Form.Label>
						Price:
					</Form.Label>
					<Form.Control
						type="number"
						placeholder="Insert Price Here" value={price}
						onChange={scene => setPrice(scene.target.value)}
						required
					/>
				</Form.Group>

				{isComplete ? 
					<Button variant="primary" className="btn btn-block" type="submit" id="submitBtn">
						Create New Product
					</Button>
					: 
					<Button variant="danger" className="btn btn-block" type="submit" id="submitBtn" disabled>
						Create New Product
					</Button>
				}
				
			</Form>
		</Container>
	);
}