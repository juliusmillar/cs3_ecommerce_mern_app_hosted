import UserContext from '../UserContext';
import { useState, useEffect, useContext } from 'react';
import { Redirect } from 'react-router-dom';
import { Form, Button, Container } from 'react-bootstrap';
import Hero from '../components/Banner';
import Swal from 'sweetalert2';

const bannerLabel = {
	title: 'Account Login',
	tagline: 'Access your account and start shopping.'
}

export default function Login() {

	const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	const authenticate = (e) => {

		e.preventDefault()
		
		fetch(`https://calm-sierra-46560.herokuapp.com/users/login`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json()) 
		.then(ConvertedInformation => {

			console.log(ConvertedInformation)
			
			if (typeof ConvertedInformation.accessToken !== "undefined") {
				localStorage.setItem('accessToken', ConvertedInformation.accessToken);
				retrieveUserDetails(ConvertedInformation.accessToken);
				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to the App!"
				})
			} else {
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check Login Details and Try Again"
				})
			}
		})
	}

	const retrieveUserDetails = (token) => {
		
		fetch('https://calm-sierra-46560.herokuapp.com/users/details', {
			headers: {
				Authorization: `Bearer ${token}` 
			} 
		}).then(resultOfPromise => resultOfPromise.json()).then(convertedResult => {
			console.log(convertedResult);
			
			setUser({
				id: convertedResult._id,
				isAdmin: convertedResult.isAdmin
			})
		})
	}

	useEffect(()=>{
		if(email !== "" && password !== ""){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password]); 

	return(

		(user.id) ? 
			<Redirect to="/products" />
		:

		<Container>
			{/*Page banner*/}
			<Hero info={bannerLabel} />

			{/*Login Form component*/}
			<Form className="mb-5" onSubmit={e => authenticate(e)}>
				{/*user Email*/}
				<Form.Group controlId="userEmail">
					<Form.Label> Email Address :</Form.Label>
					<Form.Control 
						type="email" 
						placeholder="Insert Email Here" 
						value={email}
						onChange={e => setEmail(e.target.value)}
						required/>
				</Form.Group>

				{/*user Password*/}
				<Form.Group controlId="userPassword">
					<Form.Label> Password :</Form.Label>
					<Form.Control 
						type="password" 
						placeholder="Insert Password Here" 
						value={password}
						onChange={e => setPassword(e.target.value)}
						required/>
				</Form.Group>

			{/*button component*/}
			{
				isActive ?
					<Button type="submit" id="submitBtn" variant="success" className="btn btn-block">Login</Button>
					:
					<Button type="submit" id="submitBtn" variant="success" className="btn btn-block" disabled>Login</Button>
			}
			</Form>
		</Container>
	);
}