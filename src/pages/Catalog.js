import { useState } from 'react';
import Hero from '../components/Banner';
import ProductCard from '../components/ProductCard';
import { Container } from 'react-bootstrap';


let infoBanner = {
	title: "Featured Products",
	tagline: "Start shopping now!"
}


export default function Products() {

	const [ products, setProducts ] = useState([]); 

	fetch('https://calm-sierra-46560.herokuapp.com/products/active').then(response => response.json()).then(convertedData => {
		console.log(convertedData);
		setProducts(convertedData.map(item => {
			return(
				<ProductCard key={item._id} productInfo={item}/>
			)
		}))
	})

	return(
		<Container>
			<Hero info={infoBanner} />
			{products}
		</Container>
	);
}